import { Notyf } from "notyf";
import "notyf/notyf.min.css";

window.notyf = new Notyf({
	duration: 5000,
	position: {
		x: "right",
		y: "top",
	},
	types: [
		{
			type: "default",
			backgroundColor: "#3B7DDD",
			icon: {
				className: "notyf__icon--success",
				tagName: "i",
			},
		},
		{
			type: "success",
			backgroundColor: "#28a745",
			icon: {
				className: "notyf__icon--success",
				tagName: "i",
			},
		},
		{
			type: "warning",
			backgroundColor: "#ffc107",
			icon: {
				className: "notyf__icon--error",
				tagName: "i",
			},
		},
		{
			type: "danger",
			backgroundColor: "#dc3545",
			icon: {
				className: "notyf__icon--error",
				tagName: "i",
			},
		},
	],
});

//Notify for flashes
let notificationsArray = document.querySelectorAll(".notifications-div");
if (notificationsArray.length > 0) {
	for (let i = 0; i < notificationsArray.length; i++) {
		window.notyf.open({
			type: notificationsArray[i].dataset["type"],
			message: notificationsArray[i].dataset["message"],
			duration: 3000,
			ripple: true,
			dismissible: false,
			position: {
				x: "center",
				y: "top",
			},
		});
	}
}
