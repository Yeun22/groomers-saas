const menuBtn = document.querySelector("#menu-btn");
const divMenuLinks = document.querySelector("#menu-mobile-link");

//Gérer classe active du menu

document.querySelectorAll(".navigation a").forEach((el) => {
	el.addEventListener("click", () => {
		document.querySelectorAll(".navigation a").forEach((otherEl) => {
			otherEl.classList.remove("active");
		});
		el.classList.add("active");
	});
});

//Gerer le menu mobile
divMenuLinks.classList.add("d-none");
menuBtn.addEventListener("click", () => {
	divMenuLinks.classList.toggle("d-none");
});

const linksMenu = document.querySelectorAll("#menu-mobile-link a");
linksMenu.forEach((link) => {
	link.addEventListener("click", () => {
		link.classList.add("active");
		divMenuLinks.classList.add("d-none");
	});
});

// Scroll Spy pour class active
window.addEventListener("scroll", () => {
	let currentTop = window.scrollY;
	let elems = document.querySelectorAll(".scrollspy");
	elems.forEach((el) => {
		let elemTop = el.offsetTop;
		let elemBottom = elemTop + el.offsetHeight;
		if (currentTop >= elemTop - 5 && currentTop <= elemBottom + 5) {
			let navElem = document.querySelector(
				'.navigation a[href="#' + el.id + '"]'
			);
			document.querySelectorAll(".navigation a").forEach((otherEl) => {
				otherEl.classList.remove("active");
			});
			navElem.classList.add("active");
		}
	});
});

//Onglets Produits classe active

document.querySelectorAll(".onglets-button button").forEach((btn) => {
	btn.addEventListener("click", () => {
		document.querySelectorAll(".onglets-button button").forEach((elt) => {
			elt.classList.remove("active");
		});

		btn.classList.add("active");

		let idBtn = btn.id;
		let idSplit = idBtn.split("-");
		let idDiv = idSplit[0];
		document
			.querySelectorAll(".onglets-produits")
			.forEach((div) => div.classList.add("hide"));
		document.querySelector("#" + idDiv).classList.remove("hide");
	});
});

//Galerie Photo :

const btnPrev = document.querySelector("#prev");
const btnNext = document.querySelector("#next");
const items = document.querySelectorAll(".galerie-box");
const nbSlide = items.length;
let count = 0;

function nextSlide() {
	items[count].classList.remove("active");

	if (count < nbSlide - 1) {
		count++;
	} else {
		count = 0;
	}

	items[count].classList.add("active");
}
function prevSlide() {
	items[count].classList.remove("active");

	if (count > 0) {
		count--;
	} else {
		count = nbSlide - 1;
	}

	items[count].classList.add("active");
}

btnNext.addEventListener("click", () => {
	items[count].classList.remove("active");

	if (count < nbSlide - 1) {
		count++;
	} else {
		count = 0;
	}

	items[count].classList.add("active");
});
btnPrev.addEventListener("click", () => {
	items[count].classList.remove("active");

	if (count > 0) {
		count--;
	} else {
		count = nbSlide - 1;
	}

	items[count].classList.add("active");
});

//v2 ajout d'un aggrandissement des tof
