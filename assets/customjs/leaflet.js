import "leaflet";

const groomingsData =
	document.querySelector("#groomings-array").dataset.grooming;

var map = L.map("map").setView([48.85, 2.34], 7);

var tiles = L.tileLayer(
	"https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
	{
		maxZoom: 11,
		attribution:
			'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: "mapbox/streets-v11",
		tileSize: 512,
		zoomOffset: -1,
	}
).addTo(map);

let groomings = JSON.parse(groomingsData);
groomings.forEach((g) => {
	let addressConcat = g.address.replace(" ", "+");
	let url =
		"https://api-adresse.data.gouv.fr/search/?q=" +
		addressConcat +
		"&postcode=" +
		g.postal_code +
		"&city=" +
		g.city +
		"&country=" +
		g.country;
	fetch(url)
		.then((response) => response.json())
		.then((json) => {
			let xy = json.features[0].geometry.coordinates;
			let y = xy[0];
			let x = xy[1];
			let marker = L.marker([x, y]).addTo(map);
			if (g.website_id === null) {
				marker
					.bindPopup(
						`<b>${g.name}</b> ! 
						<br>
						<i>${g.address}</i> ${g.city}
						<br>
						<b>${g.phone_number}</b>
						<br>
						<i>This Salon doesn't have a website for now</i>
						`
					)
					.openPopup();
			} else {
				marker
					.bindPopup(
						`<b>${g.name}</b> ! 
					<br>
					<i>${g.address}</i> ${g.city}
					<br><a href="/website/salon/${g.id}/groomer/${g.groomer_id}/informations">Go to Website</a>
					<br>
					<b>${g.phone_number}</b>
					`
					)
					.openPopup();
			}
		})
		.catch(() => {
			console.log("error");
		});
});
