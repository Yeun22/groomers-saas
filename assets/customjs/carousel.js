const carousel = document.querySelector("#carousel-prestations");
const figure = carousel.querySelector("figure");
const nav = carousel.querySelector("nav");
const numImages = figure.childElementCount;
const theta = (2 * Math.PI) / numImages;
let currImage = 0;

for (i = 2; i <= numImages; i++) {
	document.querySelector("figure p:nth-child(" + i + ")").style.transform =
		"rotateY(" + (i - 1) * theta + "rad)";
}

nav.addEventListener("click", (e) => {
	let c = e.target;

	if (c.tagName.toUpperCase() != "BUTTON") return;

	if (c.classList.contains("next")) {
		currImage++;
	} else {
		currImage--;
	}
	figure.style.transform = "rotateY(" + currImage * -theta + "rad)";
});
