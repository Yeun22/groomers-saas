const carousel = document.querySelector("#carousel-prestations");
const figure = carousel.querySelector("figure");
const nav = carousel.querySelector("nav");
const numImages = figure.childElementCount;
const theta = (2 * Math.PI) / numImages;
let currImage = 0;

for (i = 2; i++; i < numImages) {
	document.querySelector(
		`figure p:nth-child(${i})`
	).style.transform = `rotateY((${i} - 1) * ${theta})rad)`;
}

// nav.addEventListener("click", onClick, true);
console.log("nav");
console.log(nav);
nav.addEventListener(
	"click",
	() => {
		console.log("coucou ma bite");
	},
	true
);

function onClick(e) {
	e.stopPropagation();

	var t = e.target;
	if (t.tagName.toUpperCase() != "BUTTON") return;

	if (t.classList.contains("next")) {
		currImage++;
	} else {
		currImage--;
	}

	figure.style.transform = `rotateY((${currImage * -theta})rad)`;
}
