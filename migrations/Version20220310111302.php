<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220310111302 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE grooming_salon ADD horaires_id INT NOT NULL, ADD address_line1 VARCHAR(255) NOT NULL, ADD address_line2 VARCHAR(255) DEFAULT NULL, ADD postal_code VARCHAR(15) NOT NULL, ADD city VARCHAR(255) NOT NULL, ADD country VARCHAR(255) NOT NULL, ADD phone_number VARCHAR(15) NOT NULL');
        $this->addSql('ALTER TABLE grooming_salon ADD CONSTRAINT FK_EDD2C5938AF49C8B FOREIGN KEY (horaires_id) REFERENCES horaire_week (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EDD2C5938AF49C8B ON grooming_salon (horaires_id)');
        $this->addSql('ALTER TABLE website_informations ADD grooming_salon_id INT NOT NULL, DROP adress_line1, DROP address_line2, DROP postal_code, DROP city, DROP country, DROP phone_number, CHANGE text_presentation text_presentation LONGTEXT NOT NULL, CHANGE link_facebook link_facebook VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE website_informations ADD CONSTRAINT FK_FBB9DAF447445387 FOREIGN KEY (grooming_salon_id) REFERENCES grooming_salon (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FBB9DAF447445387 ON website_informations (grooming_salon_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE grooming_salon DROP FOREIGN KEY FK_EDD2C5938AF49C8B');
        $this->addSql('DROP INDEX UNIQ_EDD2C5938AF49C8B ON grooming_salon');
        $this->addSql('ALTER TABLE grooming_salon DROP horaires_id, DROP address_line1, DROP address_line2, DROP postal_code, DROP city, DROP country, DROP phone_number');
        $this->addSql('ALTER TABLE website_informations DROP FOREIGN KEY FK_FBB9DAF447445387');
        $this->addSql('DROP INDEX UNIQ_FBB9DAF447445387 ON website_informations');
        $this->addSql('ALTER TABLE website_informations ADD adress_line1 VARCHAR(255) NOT NULL, ADD address_line2 VARCHAR(255) DEFAULT NULL, ADD postal_code VARCHAR(10) NOT NULL, ADD city VARCHAR(255) NOT NULL, ADD country VARCHAR(255) NOT NULL, ADD phone_number VARCHAR(15) NOT NULL, DROP grooming_salon_id, CHANGE text_presentation text_presentation LONGTEXT DEFAULT NULL, CHANGE link_facebook link_facebook VARCHAR(255) DEFAULT NULL');
    }
}
