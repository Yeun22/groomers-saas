<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311100705 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE website_informations DROP FOREIGN KEY FK_FBB9DAF447445387');
        $this->addSql('DROP INDEX UNIQ_FBB9DAF447445387 ON website_informations');
        $this->addSql('ALTER TABLE website_informations DROP grooming_salon_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE website_informations ADD grooming_salon_id INT NOT NULL');
        $this->addSql('ALTER TABLE website_informations ADD CONSTRAINT FK_FBB9DAF447445387 FOREIGN KEY (grooming_salon_id) REFERENCES grooming_salon (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FBB9DAF447445387 ON website_informations (grooming_salon_id)');
    }
}
