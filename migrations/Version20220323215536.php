<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220323215536 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_groomer ADD showroom_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_groomer ADD CONSTRAINT FK_5333CF9D2243B88B FOREIGN KEY (showroom_id) REFERENCES showroom (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5333CF9D2243B88B ON user_groomer (showroom_id)');
        $this->addSql('ALTER TABLE website_informations DROP FOREIGN KEY FK_FBB9DAF42243B88B');
        $this->addSql('DROP INDEX IDX_FBB9DAF42243B88B ON website_informations');
        $this->addSql('ALTER TABLE website_informations DROP showroom_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_groomer DROP FOREIGN KEY FK_5333CF9D2243B88B');
        $this->addSql('DROP INDEX UNIQ_5333CF9D2243B88B ON user_groomer');
        $this->addSql('ALTER TABLE user_groomer DROP showroom_id');
        $this->addSql('ALTER TABLE website_informations ADD showroom_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE website_informations ADD CONSTRAINT FK_FBB9DAF42243B88B FOREIGN KEY (showroom_id) REFERENCES showroom (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_FBB9DAF42243B88B ON website_informations (showroom_id)');
    }
}
