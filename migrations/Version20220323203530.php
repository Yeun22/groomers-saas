<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220323203530 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE picture_showroom (id INT AUTO_INCREMENT NOT NULL, showroom_id INT NOT NULL, description VARCHAR(255) NOT NULL, url_picture VARCHAR(255) NOT NULL, INDEX IDX_76EFD1432243B88B (showroom_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE showroom (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE picture_showroom ADD CONSTRAINT FK_76EFD1432243B88B FOREIGN KEY (showroom_id) REFERENCES showroom (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE picture_showroom DROP FOREIGN KEY FK_76EFD1432243B88B');
        $this->addSql('DROP TABLE picture_showroom');
        $this->addSql('DROP TABLE showroom');
    }
}
