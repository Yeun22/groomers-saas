<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220919105644 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE purchase_item_product');
        $this->addSql('ALTER TABLE purchase CHANGE adress address VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE purchase_item ADD product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE purchase_item ADD CONSTRAINT FK_6FA8ED7D4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_6FA8ED7D4584665A ON purchase_item (product_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE purchase_item_product (purchase_item_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_1C0DD5594584665A (product_id), INDEX IDX_1C0DD5599B59827 (purchase_item_id), PRIMARY KEY(purchase_item_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE purchase_item_product ADD CONSTRAINT FK_1C0DD5594584665A FOREIGN KEY (product_id) REFERENCES product (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase_item_product ADD CONSTRAINT FK_1C0DD5599B59827 FOREIGN KEY (purchase_item_id) REFERENCES purchase_item (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase CHANGE address adress VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE purchase_item DROP FOREIGN KEY FK_6FA8ED7D4584665A');
        $this->addSql('DROP INDEX IDX_6FA8ED7D4584665A ON purchase_item');
        $this->addSql('ALTER TABLE purchase_item DROP product_id');
    }
}
