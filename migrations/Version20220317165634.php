<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220317165634 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE grooming_salon_service_categorie (grooming_salon_id INT NOT NULL, service_categorie_id INT NOT NULL, INDEX IDX_47D0B85247445387 (grooming_salon_id), INDEX IDX_47D0B852635992C8 (service_categorie_id), PRIMARY KEY(grooming_salon_id, service_categorie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE service_categorie (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, picture VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE grooming_salon_service_categorie ADD CONSTRAINT FK_47D0B85247445387 FOREIGN KEY (grooming_salon_id) REFERENCES grooming_salon (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE grooming_salon_service_categorie ADD CONSTRAINT FK_47D0B852635992C8 FOREIGN KEY (service_categorie_id) REFERENCES service_categorie (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE grooming_salon_service_categorie DROP FOREIGN KEY FK_47D0B852635992C8');
        $this->addSql('DROP TABLE grooming_salon_service_categorie');
        $this->addSql('DROP TABLE service_categorie');
    }
}
