<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220310103434 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE grooming_salon (id INT AUTO_INCREMENT NOT NULL, website_informations_id INT DEFAULT NULL, user_groomer_id INT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_EDD2C5931DE6387A (website_informations_id), INDEX IDX_EDD2C59360384457 (user_groomer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horaire_week (id INT AUTO_INCREMENT NOT NULL, monday VARCHAR(50) NOT NULL, tuesday VARCHAR(50) NOT NULL, wednesday VARCHAR(50) NOT NULL, thursday VARCHAR(50) NOT NULL, friday VARCHAR(50) NOT NULL, saturday VARCHAR(50) NOT NULL, sunday VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE website_informations (id INT AUTO_INCREMENT NOT NULL, adress_line1 VARCHAR(255) NOT NULL, address_line2 VARCHAR(255) DEFAULT NULL, postal_code VARCHAR(10) NOT NULL, city VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, text_presentation LONGTEXT DEFAULT NULL, phone_number VARCHAR(15) NOT NULL, email_contact VARCHAR(255) NOT NULL, link_facebook VARCHAR(255) DEFAULT NULL, link_instagram VARCHAR(255) DEFAULT NULL, link_tiktok VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE grooming_salon ADD CONSTRAINT FK_EDD2C5931DE6387A FOREIGN KEY (website_informations_id) REFERENCES website_informations (id)');
        $this->addSql('ALTER TABLE grooming_salon ADD CONSTRAINT FK_EDD2C59360384457 FOREIGN KEY (user_groomer_id) REFERENCES user_groomer (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE grooming_salon DROP FOREIGN KEY FK_EDD2C5931DE6387A');
        $this->addSql('DROP TABLE grooming_salon');
        $this->addSql('DROP TABLE horaire_week');
        $this->addSql('DROP TABLE website_informations');
    }
}
