<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220817111915 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_customer (id INT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, sex VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_customer_grooming_salon (user_customer_id INT NOT NULL, grooming_salon_id INT NOT NULL, INDEX IDX_CD6E58083A8E0A66 (user_customer_id), INDEX IDX_CD6E580847445387 (grooming_salon_id), PRIMARY KEY(user_customer_id, grooming_salon_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_customer ADD CONSTRAINT FK_61B46A09BF396750 FOREIGN KEY (id) REFERENCES user_general (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_customer_grooming_salon ADD CONSTRAINT FK_CD6E58083A8E0A66 FOREIGN KEY (user_customer_id) REFERENCES user_customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_customer_grooming_salon ADD CONSTRAINT FK_CD6E580847445387 FOREIGN KEY (grooming_salon_id) REFERENCES grooming_salon (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_customer_grooming_salon DROP FOREIGN KEY FK_CD6E58083A8E0A66');
        $this->addSql('DROP TABLE user_customer');
        $this->addSql('DROP TABLE user_customer_grooming_salon');
    }
}
