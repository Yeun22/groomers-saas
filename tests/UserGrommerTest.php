<?php

use App\Entity\UserGroomer;
use PHPUnit\Framework\TestCase;

class UserGroomerTest extends TestCase

{
    public function testIsTrue()
    {
        $userGroomer = new UserGroomer();
        $date = new DateTimeImmutable('now');
        $userGroomer->setEmail("monmail@mail.com")
            ->setCreatedAt($date)
            ->setPassword('password')
            ->setPhoneNumber("0643280096")
            ->setFirstname('Firstname')
            ->setLastname('Lastname')
            ->setSex('homme')
            ->setAccessSite(true)
            ->setAccessEcommerce(true)
            ->setAccessCrm(true);

        $this->assertTrue($userGroomer->getEmail() === 'monmail@mail.com');
        $this->assertTrue($userGroomer->getFirstname() === 'Firstname');
        $this->assertTrue($userGroomer->getLastname() === 'Lastname');
        $this->assertTrue($userGroomer->getPassword() === 'password');
        $this->assertTrue($userGroomer->getCreatedAt() === $date);
        $this->assertTrue($userGroomer->getPhoneNumber() === "0643280096");
        $this->assertTrue($userGroomer->getSex() === "homme");
        $this->assertTrue($userGroomer->getAccessCrm());
        $this->assertTrue($userGroomer->getAccessEcommerce());
        $this->assertTrue($userGroomer->getAccessSite());
    }
    public function testIsFalse()
    {
        $userGroomer = new UserGroomer();
        $date = new DateTimeImmutable('now');
        $userGroomer->setEmail("monmail@mail.com")
            ->setCreatedAt($date)
            ->setPassword('password')
            ->setPhoneNumber("0643280096")
            ->setSex('homme')
            ->setAccessSite(false)
            ->setAccessEcommerce(false)
            ->setAccessCrm(false);

        $this->assertFalse($userGroomer->getEmail() === 'monmail@pasmail.com');
        $this->assertFalse($userGroomer->getFirstname() === 'prénom');
        $this->assertFalse($userGroomer->getLastname() === 'nom');
        $this->assertFalse($userGroomer->getPassword() === '1234');
        $this->assertFalse($userGroomer->getCreatedAt() === new DateTimeImmutable("1997-12-13"));
        $this->assertFalse($userGroomer->getPhoneNumber() === "0600552423");
        $this->assertFalse($userGroomer->getSex() === "femme");
        $this->assertTrue($userGroomer->getAccessCrm());
        $this->assertTrue($userGroomer->getAccessEcommerce());
        $this->assertTrue($userGroomer->getAccessSite());
    }
}
