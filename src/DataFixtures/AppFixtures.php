<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Product;
use App\Entity\UserGroomer;
use App\Entity\GroomingSalon;
use App\Entity\HoraireWeek;
use App\Entity\ProductCategorie;
use App\Entity\ServiceCategorie;
use App\Entity\UserCustomer;
use App\Entity\WebsiteInformations;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private  UserPasswordHasherInterface $hasheur)
    {
    }

    public function load(ObjectManager $manager,): void
    {
        // 0 Initialiser FAKER
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new \Liior\Faker\Prices($faker));
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
        $faker->addProvider(new \Bluemmb\Faker\PicsumPhotosProvider($faker));

        // 1 Créer un groomer
        $groomer = new UserGroomer();
        $groomer->setEmail("test@test.com")
            ->setFirstname("Yeun")
            ->setLastname("LE FLOCH")
            ->setPassword($this->hasheur->hashPassword(
                $groomer,
                "password"
            ))
            ->setPhoneNumber("0643280096")
            ->setSex('homme');

        $manager->persist($groomer);

        // Et un client : 
        $customer = new UserCustomer();
        $customer->setEmail("user@mail.com")->setFirstname("Toto")->setLastname("LE RIGOLO")->setPhoneNumber("0786289622")->setPassword($this->hasheur->hashPassword(
            $customer,
            "password"
        ));

        // 2 Créer un salon 

        // 2b Créer des horaires : 
        $hourly = new HoraireWeek();
        $hourly->setMonday("9h-18h")
            ->setTuesday("9h-18h")
            ->setWednesday("9h-18h")
            ->setThursday("9h-18h")
            ->setSaturday("9h-18h")
            ->setSunday("9h-18h");

        $salon = new GroomingSalon();

        $salon->setUserGroomer($groomer)
            ->setAddressLine1("4 le cosquer")
            ->setCity("BOQUEHO")
            ->setPostalCode("22170")
            ->setCountry("FRANCE")
            ->setName("Esthétichien")
            ->setPhoneNumber("0643280096")
            ->setHourly($hourly->getWeekHourly());
        $manager->persist($salon);

        //3 Créer des categories et des produits 
        $product_accessory = new ProductCategorie();
        $product_accessory->setName('Accessoire')->setDescription("Tous les accessoires pour vos animaux : Laisses, colliers, panniers, ...");
        $manager->persist($product_accessory);

        for ($p = 0; $p < mt_rand(5, 15); $p++) {
            $product = new Product;
            $product->setName($faker->productName())
                ->setPrice($faker->price(4000, 20000))
                // ->setSlug(strtolower($this->slugger->slug($product->getName())))
                ->setCategorie($product_accessory)
                ->setGroomingSalon($salon)
                ->setUrlPicture($faker->imageUrl(400, 400, true))
                ->setDescription($faker->paragraph());

            $products[] = $product;

            $manager->persist($product);
        }

        $product_cosmetic = new ProductCategorie();
        $product_cosmetic->setName('Cosmétique')->setDescription("Tous les cosmétiques pour vos animaux : Shampoings, parfums, démêlants, ...");
        $manager->persist($product_cosmetic);
        for ($p = 0; $p < mt_rand(5, 15); $p++) {
            $product = new Product;
            $product->setName($faker->productName())
                ->setPrice($faker->price(4000, 20000))
                // ->setSlug(strtolower($this->slugger->slug($product->getName())))
                ->setCategorie($product_cosmetic)
                ->setGroomingSalon($salon)
                ->setUrlPicture($faker->imageUrl(400, 400, true))
                ->setDescription($faker->paragraph());

            $products[] = $product;

            $manager->persist($product);
        }
        $product_food = new ProductCategorie();
        $product_food->setName('Alimentation')->setDescription("L'alimentation de vos animaux : Croquettes, friandises, os, ...");
        $manager->persist($product_food);
        for ($p = 0; $p < mt_rand(5, 15); $p++) {
            $product = new Product;
            $product->setName($faker->productName())
                ->setPrice($faker->price(4000, 20000))
                // ->setSlug(strtolower($this->slugger->slug($product->getName())))
                ->setCategorie($product_food)
                ->setGroomingSalon($salon)
                ->setUrlPicture($faker->imageUrl(400, 400, true))
                ->setDescription($faker->paragraph());

            $products[] = $product;

            $manager->persist($product);
        }
        $product_toys = new ProductCategorie();
        $product_toys->setName('Jouets')->setDescription("Divertissez vos compagnons ; Corde, peluches, balles, ...");
        $manager->persist($product_toys);
        for ($p = 0; $p < mt_rand(5, 15); $p++) {
            $product = new Product;
            $product->setName($faker->productName())
                ->setPrice($faker->price(4000, 20000))
                // ->setSlug(strtolower($this->slugger->slug($product->getName())))
                ->setCategorie($product_toys)
                ->setGroomingSalon($salon)
                ->setUrlPicture($faker->imageUrl(400, 400, true))
                ->setDescription($faker->paragraph());

            $products[] = $product;

            $manager->persist($product);
        }
        $product_other = new ProductCategorie();
        $product_other->setName('Divers')->setDescription("Les inclassables mais tout autant utiles:  Habits, sacs, transport, ...");
        $manager->persist($product_other);
        for ($p = 0; $p < mt_rand(5, 15); $p++) {
            $product = new Product;
            $product->setName($faker->productName())
                ->setPrice($faker->price(4000, 20000))
                // ->setSlug(strtolower($this->slugger->slug($product->getName())))
                ->setCategorie($product_other)
                ->setGroomingSalon($salon)
                ->setUrlPicture($faker->imageUrl(400, 400, true))
                ->setDescription($faker->paragraph());

            $products[] = $product;

            $manager->persist($product);
        }
        //4 Créer des services

        $service_bain = new ServiceCategorie();
        $service_bain->setName('Bain')->setPicture('/images/categorie_service_img/bain.jpg');
        $manager->persist($service_bain);

        $service_coupeCiseaux = new ServiceCategorie();
        $service_coupeCiseaux->setName('Coupe Ciseaux')->setPicture('/images/categorie_service_img/coupe_ciseaux.jpg');
        $manager->persist($service_coupeCiseaux);

        $service_tonte = new ServiceCategorie();
        $service_tonte->setName('Tonte')->setPicture('/images/categorie_service_img/tonte.jpg');
        $manager->persist($service_tonte);

        $service_demelage = new ServiceCategorie();
        $service_demelage->setName('Démêlage')->setPicture('/images/categorie_service_img/démêlage.jpg');
        $manager->persist($service_demelage);

        $service_couleur = new ServiceCategorie();
        $service_couleur->setName('Couleur')->setPicture('/images/categorie_service_img/couleur.jpg');
        $manager->persist($service_couleur);

        $service_rectifications = new ServiceCategorie();
        $service_rectifications->setName('Rectifications')->setPicture('/images/categorie_service_img/bain_rectifs.jpg');
        $manager->persist($service_rectifications);

        // 5 Créer un website
        $website = new WebsiteInformations();
        $website->setEmailContact("test@test.com")
            ->setLinkFacebook("https://facebook.com/bobetseshumains")
            ->setTextPresentation("Coucouc'est moi")
            ->setGroomingSalon($salon);
        $manager->persist($website);
        $manager->flush();
    }
}
