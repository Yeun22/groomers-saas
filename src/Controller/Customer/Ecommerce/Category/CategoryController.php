<?php

namespace App\Controller\Customer\Ecommerce\Category;

use App\Repository\ProductCategorieRepository;
use App\Repository\ProductRepository;
use App\Services\GetAllInformationsGroomingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/e-shop/category')]

class CategoryController extends AbstractController
{

    public function __construct(
        private ProductCategorieRepository $productCategorie,
        private ProductRepository $productRepository,
        private GetAllInformationsGroomingService $getAllInformationsGroomingService
    ) {
    }

    #[Route('/{idCategory}/groomingSalon/{idSalon}-{idGroomer}', name: 'category_details')]
    public function showDetailsProduct($idSalon, $idCategory, $idGroomer)
    {
        $category = $this->productCategorie->findOneById($idCategory);
        // $salon = $this->getAllInformationsGroomingService->renderAllInformationInSalon($idSalon, $idGroomer);
        $products = $this->productRepository->findBy(['categorie' => $category, 'groomingSalon' => $idSalon]);

        return $this->render('customers/eshop/category/category_products.html.twig', [
            'category' => $category,
            'products' => $products,
            'idSalon' => $idSalon,
            'idGroomer' => $idGroomer,
        ]);
    }
}
