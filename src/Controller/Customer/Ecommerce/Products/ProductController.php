<?php

namespace App\Controller\Customer\Ecommerce\Products;

use App\Repository\ProductRepository;
use App\Services\GetAllInformationsGroomingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/e-shop/products')]

class ProductController extends AbstractController
{

    public function __construct(
        private ProductRepository $productRepository,
        private GetAllInformationsGroomingService $getAllInformationsGroomingService
    ) {
    }

    #[Route('/{idProduct}/groomingSalon/{idSalon}-{idGroomer}', name: 'product_details')]
    public function showDetailsProduct($idSalon, $idProduct, $idGroomer)
    {
        $product = $this->productRepository->findOneBy(['id' => $idProduct, 'groomingSalon' => $idSalon]);
        $salon = $this->getAllInformationsGroomingService->renderAllInformationInSalon($idSalon, $idGroomer);
        return $this->render('customers/eshop/product/product_details.html.twig', [
            'product' => $product,
            'salon' => $salon,
            'idSalon' => $idSalon,
            'idGroomer' => $idGroomer
        ]);
    }
}
