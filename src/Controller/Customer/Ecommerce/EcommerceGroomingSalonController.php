<?php

namespace App\Controller\Customer\Ecommerce;

use Exception;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\GetAllInformationsGroomingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/e-shop')]
class EcommerceGroomingSalonController extends AbstractController
{
    public function __construct(
        private GetAllInformationsGroomingService $groomingSalonService
    ) {
    }

    #[Route("/groomingsalon{idSalon}-{idGroomer}/home", name: 'eshop_homepage')]
    public function homepage_eshop_render_customer(int $idSalon, int $idGroomer)
    {
        try {
            $salon = $this->groomingSalonService->renderAllInformationInSalon($idSalon, $idGroomer);
        } catch (Exception $e) {
            $this->addFlash('danger', $e->getMessage());
            return $this->redirectToRoute('customer_search_groomer');
        }

        return $this->render('customers/eshop/homepage.html.twig', [
            'salon' => $salon,
            'idSalon' => $idSalon,
            'idGroomer' => $idGroomer,
        ]);
    }

    #[Route('/groomingsalon{idSalon}-{idGroomer}/about', name: 'eshop_aboutpage')]
    public function about_eshop_page(int $idSalon, int $idGroomer)
    {
        try {
            $salon = $this->groomingSalonService->renderAllInformationInSalon($idSalon, $idGroomer);
        } catch (Exception $e) {
            $this->addFlash('danger', $e->getMessage());
            return $this->redirectToRoute('customer_search_groomer');
        }

        $mapsResearch = $salon['salon']->getAddressLine1() . "+," . $salon['salon']->getPostalCode() . "+," . $salon['salon']->getCity();
        $mapsResearch = str_replace(" ", "+", $mapsResearch,);


        return $this->render('customers/eshop/about.html.twig', [
            'salon' => $salon['salon'],
            'groomer' => $salon['groomer'],
            'website' => $salon['website'],
            'idSalon' => $idSalon,
            'idGroomer' => $idGroomer,
            'mapsResearch' => $mapsResearch
        ]);
    }
}
