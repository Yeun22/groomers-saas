<?php

namespace App\Controller\Customer\Ecommerce;


use App\Cart\CartService;
use App\Form\CartConfirmationType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

#[Route('/e-shop')]
class CartController extends AbstractController
{
    public function __construct(
        private ProductRepository $productRepository,
        private CartService $cartService,
    ) {
    }

    #[Route("/cart/add/{idProduct}/groomingSalon/{idSalon}-{idGroomer}", name: "cart_add", requirements: ["id" => "\d+"])]
    public function add(int $idProduct, int $idSalon, int $idGroomer, Request $request): Response
    {
        //Product exist ?
        $product = $this->productRepository->find($idProduct);

        if (!$product) {
            throw $this->createNotFoundException("Le produit $idProduct n'existe pas");
        }
        // Add
        $this->cartService->add($idProduct);

        $this->addFlash('success', "Le produit à bien été ajouté au pannier");

        //If We are on Cart Page
        if ($request->query->get('returnToCart')) {
            return $this->redirectToRoute("cart_show", [
                'idSalon' => $idSalon,
                'idGroomer' => $idGroomer
            ]);
        }
        //Else we go to product page
        return $this->redirectToRoute('product_details', [
            'idProduct' => $idProduct,
            'idSalon' => $idSalon,
            'idGroomer' => $idGroomer
        ]);
    }

    #[Route("/cart/grooming-salon{idSalon}-{idGroomer}", name: "cart_show")]
    public function show($idSalon, $idGroomer)
    {
        $form = $this->createForm(CartConfirmationType::class);

        $detailedCart = $this->cartService->getDetailedCartItems();
        $total = $this->cartService->getTotal();

        return $this->render('customers/eshop/cart/index.html.twig', [
            'items' => $detailedCart,
            'total' => $total,
            'idSalon' => $idSalon,
            'idGroomer' => $idGroomer,
            'confirmationForm' => $form->createView()
        ]);
    }


    #[Route("/cart/delete/{idProduct}/groomingSalon/{idSalon}-{idGroomer}", name: "cart_delete", requirements: ["id" => "\d+"])]
    public function delete(int $idProduct, int $idSalon, int $idGroomer)
    {
        $product = $this->productRepository->find($idProduct);

        if (!$product) {
            throw $this->createNotFoundException("Le produit $idProduct n'existe pas et ne peut être supprimé");
        }
        $this->cartService->remove($idProduct);

        $this->addFlash("success", "Le produit à bien été supprimé du pannier");
        return $this->redirectToRoute("cart_show", [
            'idSalon' => $idSalon,
            'idGroomer' => $idGroomer
        ]);;
    }

    #[Route("/cart/decrement/{idProduct}/groomingSalon/{idSalon}-{idGroomer}", name: "cart_decrement", requirements: ["id" => "\d+"])]
    public function decrement(int $idProduct, int $idSalon, int $idGroomer)
    {
        $product = $this->productRepository->find($idProduct);

        if (!$product) {
            throw $this->createNotFoundException("Le produit $idProduct n'existe pas et ne peut être enlevé");
        }
        $this->cartService->decrement($idProduct);
        $this->addFlash("success", "Le nombre de produit produit à bien été réduit");

        return $this->redirectToRoute("cart_show", [
            'idSalon' => $idSalon,
            'idGroomer' => $idGroomer
        ]);
    }
}
