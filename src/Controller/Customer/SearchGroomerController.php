<?php

namespace App\Controller\Customer;

use App\Repository\GroomingSalonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


class SearchGroomerController extends AbstractController
{
    public function __construct(
        private GroomingSalonRepository $groomingSalonRepository,
    ) {
    }

    #[Route('/customers/search/groomer', name: 'customer_search_groomer')]
    public function findgroomer(Request $request): Response
    {
        $groomings = [];
        if ($request->getMethod() === "POST") {
            $research = $request->request->get('localisation-input');
            $groomings = $this->groomingSalonRepository->findBy(['city' => $research]);
            if ($groomings === []) {
                $groomings = $this->groomingSalonRepository->findBy(['postalCode' => $research]);
                if ($groomings === []) {
                    $this->addFlash("error", "We don't find your research, try something else");
                    $groomings = $this->groomingSalonRepository->findAll();
                }
            }
        } else {
            $groomings = $this->groomingSalonRepository->findAll();
        }
        $renderInfo = [];
        foreach ($groomings as $g) {
            $g->getWebsiteInformations()[0] == null ? $websiteId = null : $websiteId = $g->getWebsiteInformations()[0]->getId();
            $renderInfo[] = [
                "id" => $g->getId(),
                "name" => $g->getName(),
                "address" => $g->getAddressLine1() . " " . $g->getAddressLine2(),
                "postal_code" => $g->getPostalCode(),
                "city" => $g->getCity(),
                "country" => $g->getCountry(),
                "hourly" => $g->getHourly(),
                "website_id" => $websiteId,
                "phone_number" => $g->getPhoneNumber(),
                "groomer_id" => $g->getUserGroomer()->getId()
            ];
        }
        return $this->render('customers/search_groomer.html.twig', ['groomings' => $renderInfo]);
    }
}
