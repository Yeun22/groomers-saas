<?php

namespace App\Controller\Customer;

use Symfony\Component\Mime\Address;
use App\Repository\ProductRepository;
use App\Repository\ShowroomRepository;
use App\Repository\UserGroomerRepository;
use App\Repository\GroomingSalonRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use App\Repository\ProductCategorieRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\WebsiteInformationsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;


class WebsiteGroomingSalonController extends AbstractController
{
    public function __construct(
        private GroomingSalonRepository $salonRepository,
        private UserGroomerRepository $userGroomerRepository,
        private WebsiteInformationsRepository $websiteInformationsRepository,
        private ShowroomRepository $showroomRepository,
        private ProductCategorieRepository $productCategorieRepository,
        private ProductRepository $productRepository
    ) {
    }

    #[Route('/website/salon/{idSalon}/groomer/{idGroomer}/informations', name: 'customers_watch_website')]
    public function show_website_for_customers($idSalon, $idGroomer): Response
    {
        $groomer = $this->userGroomerRepository->findOneById($idGroomer);
        $salon = $this->salonRepository->findOneBy(["id" => $idSalon, 'userGroomer' => $groomer]);
        if ($salon->getWebsiteInformations()[0] === null) {
            $this->addFlash('danger', "Woups ! This groomer doesn't have a website");
            return $this->redirectToRoute('customer_search_groomer');
        }
        $websiteId = $salon->getWebsiteInformations()[0]->getId();
        $website = $this->websiteInformationsRepository->findOneById($websiteId);

        $mapsResearch = $salon->getAddressLine1() . "+," . $salon->getPostalCode() . "+," . $salon->getCity();
        $mapsResearch = str_replace(" ", "+", $mapsResearch,);

        $groomer->getShowroom() == null ? $showroom = false : $showroom = true;
        $products = $salon->getProducts()->toArray();

        $categories = [];
        foreach ($this->productCategorieRepository->findAll() as $c) {
            $products = $this->productRepository->findBy(['groomingSalon' => $salon, 'categorie' => $c], null, 6);

            $categories[] = ['name' => $c->getName(), 'products' => $products];
        }

        $colors = [
            'mainBg' => $website->getColors()[0]['colors-mainBg'],
            'secondBg' => $website->getColors()[1]['colors-secondBg'],
            'mainText' => $website->getColors()[2]['colors-mainText'],
            'primary' => $website->getColors()[3]['colors-primary'],
            'secondary' => $website->getColors()[4]['colors-secondary'],
        ];
        return $this->render(
            'customers/showWebsiteGrooming.html.twig',
            [
                'salon' => $salon,
                'website' => $website,
                'groomer' => $groomer,
                'mapResearch' => $mapsResearch,
                'colors' => $colors,
                "showroom" => $showroom,
                'categories' => $categories
            ]
        );
    }

    #[Route('/website/{idSalon}/groomer/{idGroomer}/informations/sending-mail', name: 'customers_watch_website_send_mail')]
    public function send_mail(Request $request, $idSalon, $idGroomer, MailerInterface $mailer): Response
    {
        $errors = [];
        $groomer = $this->userGroomerRepository->findOneById($idGroomer);
        $salon = $this->salonRepository->findOneBy(["id" => $idSalon, 'userGroomer' => $groomer]);
        $form = $request->request->get('form');
        $send = true;

        if ($form['firstname'] == "") {
            $errors['firstname'] = "Nous devons avoir votre prénom";
            $send = false;
        }
        if ($form['name'] == "") {
            $errors['name'] = "Nous devons avoir votre nom";
            $send = false;
        }
        if ($form['email'] == "") {
            $errors['email'] = "Nous devons avoir votre email";
            $send = false;
        }
        if ($form['phone'] == "") {
            $errors['phone'] = "Nous devons avoir votre numéro de téléphone";
            $send = false;
        }
        if ($form['message'] == "") {
            $error['message'] = "Vous avez oublier de l'aisser un message";
            $send = false;
        }
        if ($send) {
            $email = (new TemplatedEmail());
            $email->from($form['email'])
                ->to($salon->getWebsiteInformations()[0]->getEmailContact())
                ->subject('Un message de votre site : ' . $form['choice'])
                // path of the Twig template to render
                ->htmlTemplate('emails/send_from_your_webpage.html.twig')
                // pass variables (name => value) to the template
                ->context([
                    'firstnameCustomers' => $form['firstname'],
                    'lastnameCustomers' => $form['name'],
                    'emailCustomers' => $form['email'],
                    'phone' => $form['phone'],
                    'message' => $form['message'],
                    'groomerFirstname' => $groomer->getFirstname(),
                    'groomerLastname' => $groomer->getLastname(),
                ]);




            try {
                $mailer->send($email);
                $this->addFlash('success', "Le mail à bien été envoyé");
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('danger', "Le mail n'a pas été envoyé, veuillez ré-essayer !");
            }
        }

        return $this->redirectToRoute(
            'customers_watch_website',
            [
                "idSalon" => $idSalon,
                "idGroomer" => $idGroomer,
            ]
        );
    }
}
