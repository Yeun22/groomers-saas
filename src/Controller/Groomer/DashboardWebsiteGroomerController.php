<?php

namespace App\Controller\Groomer;

use App\Entity\Colors;
use App\Entity\UserGroomer;
use App\Entity\WebsiteInformations;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\WebsiteInformationsFormType;
use App\Repository\UserGroomerRepository;
use App\Repository\GroomingSalonRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\WebsiteInformationsRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class DashboardWebsiteGroomerController extends AbstractController
{
    public function __construct(
        private UserGroomerRepository $groomerRepository,
        private Security $security,
        private GroomingSalonRepository $salonRepository,
        private WebsiteInformationsRepository $websiteRepository,
        private EntityManagerInterface $em,
        private SessionInterface $session
    ) {
    }

    #[Route('/groomers/dashboard/{idGroomer}/website', name: 'groomers_dashboard_website_choose')]
    public function index_groomers_dashboard_website($idGroomer): Response
    {
        /** @var UserGroomer */
        $groomer = $this->groomerRepository->findOneById($idGroomer);

        // First Check if it's good user
        if ($this->security->getUser() !== $groomer) {
            $this->addFlash("warning", "Something strange happened ! Where are you go ?");
            return $this->redirectToRoute('app_logout');
        }

        $salons = $groomer->getGroomingSalons();
        $noWebsite = true;
        foreach ($salons as $salon) {
            if (!empty($salon->getWebsiteInformations())) {
                $noWebsite = false;
            }
        }
        return $this->render(
            'groomers/website/website-choose.html.twig',
            [
                'salons' => $salons,
                'idGroomer' => $groomer->getId(),
                'noWebsite' => $noWebsite
            ]
        );
    }

    #[Route('/groomers/dashboard/{idGroomer}/website/{idSalon}', name: 'groomers_dashboard_website_manage')]
    public function manage_groomers_dashboard_website($idGroomer, $idSalon, Request $request): Response
    {
        $groomer = $this->groomerRepository->findOneById($idGroomer);
        $salon = $this->salonRepository->findOneBy(['id' => $idSalon, 'userGroomer' => $groomer]);
        // $salon = $this->salonRepository->findOneById($idSalon);
        $websites = $salon->getWebsiteInformations();
        $website = $websites[0];
        $creation = false;

        if ($website === null) {
            $creation = true;
            $website = new WebsiteInformations();
        }
        $form = $this->createForm(WebsiteInformationsFormType::class, $website);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($creation) {
                    $website->setGroomingSalon($salon);
                    $this->addFlash("success", "Your website for your salon is successfully created ! ");
                }
                $this->em->persist($website);
                $this->em->flush();
                return $this->redirectToRoute(
                    'groomers_dashboard_website_addcolor',
                    ['idWebsite' => $website->getId()]
                );
            }
        }

        return $this->render(
            'groomers/website/website-manage.html.twig',
            [
                'formWebsite' => $form->createView(),
            ]
        );
    }

    #[Route('/groomers/dashboard/website/{idWebsite}/add-color', name: 'groomers_dashboard_website_addcolor')]
    public function add_color(Request $request, $idWebsite): Response
    {
        $website = $this->websiteRepository->findOneById($idWebsite);
        if ($request->getMethod() === "POST") {
            $colors = [];
            foreach ($request->request as $key => $value) {
                $colors[] = [$key => $value];
            }
            $website->setColors($colors);

            $this->em->flush();

            $this->addFlash("success", "Your website's colors for your salon are successfully updated ! ");
            $this->session->clear();

            return $this->redirectToRoute(
                'groomers_dashboard_website_medias',
                ['idWebsite' => $idWebsite]
            );
        } else {

            $colors = new Colors();
            $colorsToChoose = $colors->colors;

            //Treatment of colors website
            $colorsWebsite = [
                'mainBg' => $website->getColors()[0]['colors-mainBg'],
                'secondBg' => $website->getColors()[1]['colors-secondBg'],
                'mainText' => $website->getColors()[2]['colors-mainText'],
                'primary' => $website->getColors()[3]['colors-primary'],
                'secondary' => $website->getColors()[4]['colors-secondary'],
            ];
            return $this->render(
                'groomers/website/website-manage.html.twig',
                [
                    "website" => $website,
                    "colorsToChoose" => $colorsToChoose,
                    "colorsWebsite" => $colorsWebsite
                ]
            );
        }
    }

    #[Route('/groomers/dashboard/website/{idWebsite}/medias', name: 'groomers_dashboard_website_medias')]
    public function manage_media(Request $request, $idWebsite): Response
    {
        $website = $this->websiteRepository->findOneById($idWebsite);
        if ($request->getMethod() === 'POST') {
            $logo = $request->files->get('logo');
            $profilPicture = $request->files->get('profilPicture');
            if ($logo !== null) {
                $safeFilename = "Logo-" . $website->getId() . '.png';
                try {
                    $logo->move(
                        "./images/websites_img",
                        $safeFilename
                    );
                    $this->addFlash('success', "Logo Updated");
                } catch (FileException $e) {
                    $this->addFlash('danger', "Erreur survenue : " . $e->getMessage());
                }

                $website->setLogo($safeFilename);
            }
            if ($profilPicture !== null) {
                $safeFilename = "ProfilPicture-" . $website->getId() . '.png';
                try {
                    $profilPicture->move(
                        "./images/websites_img",
                        $safeFilename
                    );
                    $this->addFlash('success', "Profil Picture Updated");
                } catch (FileException $e) {
                    $this->addFlash('danger', "Erreur survenue : " . $e->getMessage());
                }
                $website->setProfilPicture($safeFilename);
            }
            $this->em->flush();
            return $this->redirectToRoute('groomers_dashboard');
        } else {
            return $this->render(
                'groomers/website/website-manage.html.twig',
                [
                    "website" => $website,
                ]
            );
        }
    }

    #[Route('/groomers/dashboard/{idGroomer}/website/{idSalon}/delete', name: 'groomers_dashboard_website_delete')]
    public function delete_groomers_dashboard_website($idGroomer, $idSalon, Request $request): Response
    {
        $salon = $this->salonRepository->findOneById($idSalon);
        $websites = $salon->getWebsiteInformations();
        $website = $websites[0];
        $this->websiteRepository->remove($website);
        $this->em->flush();
        $this->addFlash("success", "We remove your website successfully");
        return $this->redirectToRoute('groomers_dashboard_website_choose', ['idGroomer' => $idGroomer]);
    }
}
