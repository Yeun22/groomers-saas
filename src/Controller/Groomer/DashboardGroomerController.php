<?php

namespace App\Controller\Groomer;

use App\Repository\UserGroomerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class DashboardGroomerController extends AbstractController
{
    #[Route('/groomers/dashboard', name: 'groomers_dashboard')]
    public function index_groomers_dashboard(Security $security): Response
    {
        if ($security->getUser() === null) {
            $this->addFlash('danger', 'Please Log In before access your dashboard');
            return $this->redirectToRoute('app_home');
        } {
            return $this->render('groomers/dashboard.html.twig', []);
        }
    }
}
