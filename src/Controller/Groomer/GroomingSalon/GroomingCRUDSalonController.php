<?php

namespace App\Controller\Groomer\GroomingSalon;

use App\Entity\HoraireWeek;
use App\Entity\GroomingSalon;
use App\Form\GroomingSalonType;
use App\Form\HoraireWeekFormType;
use App\Repository\GroomingSalonRepository;
use App\Repository\ServiceCategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GroomingCRUDSalonController extends AbstractController
{
    public function __construct(
        private Security $security,
        private SessionInterface $session,
        private EntityManagerInterface $em,
        private GroomingSalonRepository $salonRepository,
        private ServiceCategorieRepository $serviceCategorieRepository
    ) {
    }

    #[Route('/groomers/dashboard/grooming-salon/create', name: 'groomers_groomingsalon_create')]
    public function create_grooming_salon(Request $request): Response
    {
        $groomingSalon = new GroomingSalon();

        $form = $this->createForm(GroomingSalonType::class, $groomingSalon);

        $form->handleRequest($request);
        if (($form->isSubmitted() && $form->isValid())) {
            $this->session->set('groomingSalon', $groomingSalon);
            return $this->redirectToRoute(
                'groomers_groomingsalon_addServices'
            );
        }

        return $this->render(
            'groomers/groomingSalon/create.html.twig',
            [
                'formGrooming' => $form->createView(),
            ]
        );
    }
    #[Route('/groomers/dashboard/grooming-salon/{idSalon}/edit', name: 'groomers_groomingsalon_edit')]
    public function edit_grooming_salon(Request $request, $idSalon): Response
    {
        $groomer = $this->security->getUser();
        $groomingSalon = $this->salonRepository->findOneBy(['id' => $idSalon, 'userGroomer' => $groomer]);
        $form = $this->createForm(GroomingSalonType::class, $groomingSalon);

        $form->handleRequest($request);
        if (($form->isSubmitted() && $form->isValid())) {
            $this->em->persist($groomingSalon);
            $this->em->flush();

            $this->session->set('groomingSalon', $groomingSalon);

            return $this->redirectToRoute(
                'groomers_groomingsalon_addServices'
            );
        }

        return $this->render(
            'groomers/groomingSalon/create.html.twig',
            [
                'formGrooming' => $form->createView(),
            ]
        );
    }
    #[Route('/groomers/dashboard/grooming-salon/add-services', name: 'groomers_groomingsalon_addServices')]
    public function create_grooming_salon_addservices(Request $request): Response
    {
        $groomer = $this->security->getUser();
        $salon = $this->session->get('groomingSalon');
        $salon->getId() === null ? $groomingSalon = $salon : $groomingSalon = $this->salonRepository->findOneBy(['id' => $salon->getId(), 'userGroomer' => $groomer]);
        $services = $groomingSalon->getServicesCategories()->toArray();
        if ($request->getMethod() === 'POST') {

            $services = [];
            foreach ($request->request as $key => $value) {
                $services[] = $value;
            }

            $services_checked = $services;
            $this->session->set('services_checked', $services_checked);

            return $this->redirectToRoute(
                'groomers_groomingsalon_addhourly'
            );
        }

        return $this->render(
            'groomers/groomingSalon/addServices.html.twig',
            [
                'groomingSalon' => $groomingSalon,
                'categories_services' => $this->serviceCategorieRepository->findAll(),
                'services' => $services
            ]
        );
    }
    #[Route('/groomers/dashboard/grooming-salon/add-hourly', name: 'groomers_groomingsalon_addhourly')]
    public function create_grooming_salon_addhourly(Request $request, $edit = false): Response
    {

        $groomer = $this->security->getUser();
        $salon = $this->session->get('groomingSalon');
        $salon->getId() === null ? $groomingSalon = $salon : $groomingSalon = $this->salonRepository->findOneBy(['id' => $salon->getId(), 'userGroomer' => $groomer]);
        $hourlyArray = $groomingSalon->getHourly();

        $hourly = new HoraireWeek();
        if (!empty($hourlyArray)) {
            $hourly->setMonday($hourlyArray['monday'])
                ->setTuesday($hourlyArray['tuesday'])
                ->setWednesday($hourlyArray['wednesday'])
                ->setThursday($hourlyArray['thursday'])
                ->setFriday($hourlyArray['friday'])
                ->setSaturday($hourlyArray['saturday'])
                ->setSunday($hourlyArray['sunday']);
            $edit = true;
        }
        $formHourly = $this->createForm(HoraireWeekFormType::class, $hourly);
        $formHourly->handleRequest($request);
        if ($formHourly->isSubmitted() && $formHourly->isValid()) {
            if (!$edit) {
                /** @var UserGroomer */
                $user = $this->security->getUser();
                $user->addGroomingSalon($groomingSalon);
                $this->em->persist($user);
            }
            if (!$edit) {
                $this->em->persist($groomingSalon);
            }

            if (!empty($groomingSalon->getServicesCategories())) {
                foreach ($groomingSalon->getServicesCategories() as $service)
                    $groomingSalon->removeServicesCategory($service);
            }

            $services_checked = $this->session->get('services_checked');
            foreach ($services_checked as $serviceId) {
                $service = $this->serviceCategorieRepository->findOneById($serviceId);
                $groomingSalon->addServicesCategory($service);
            }

            $groomingSalon->setHourly($hourly->getWeekHourly());
            $this->em->persist($groomingSalon);
            $this->em->flush();

            $this->addFlash("success", "Your grooming salon is successfully added to your account ! ");

            return $this->redirectToRoute('groomers_dashboard');
        }

        return $this->render(
            'groomers/groomingSalon/create.html.twig',
            [
                'formHourly' => $formHourly->createView(),
            ]
        );
    }

    #[Route('/groomers/dashboard/grooming-salon/{idSalon}/delete', name: 'groomers_groomingsalon_delete')]
    public function delete_grooming_salon(Request $request, $idSalon): Response
    {
        try {
            $this->salonRepository->remove($this->salonRepository->findOneById($idSalon));
            $this->addFlash('success', 'Votre salon à été supprimé');
        } catch (Exception $e) {
            $this->addFlash('danger', "Une erreur est survenue veuillez réessayer");
        }
        return $this->redirectToRoute('groomers_groomingsalon_show');
    }
}
