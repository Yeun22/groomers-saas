<?php

namespace App\Controller\Groomer\GroomingSalon;

use App\Repository\GroomingSalonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class GroomingSalonController extends AbstractController
{
    public function __construct(
        private Security $security,
        private GroomingSalonRepository $groomingSalonRepository
    ) {
    }

    #[Route('groomers/dashboard/grooming-salon/show', name: 'groomers_groomingsalon_show')]
    public function show_grooming_salon(): Response
    {
        $salons = $this->groomingSalonRepository->findBy(['userGroomer' => $this->security->getUser()]);
        return $this->render('groomers/groomingSalon/show.html.twig', ['salons' => $salons]);
    }
}
