<?php

namespace App\Controller\Groomer\GroomingSalon;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Repository\GroomingSalonRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use App\Repository\ProductCategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ProductController extends AbstractController
{

    public function __construct(
        private Security $security,
        private ProductRepository $productRepository,
        private ProductCategorieRepository $productCategorieRepository,
        private GroomingSalonRepository $salonRepository,
        private EntityManagerInterface $em
    ) {
    }
    #[Route('/groomers/dashboard/grooming-salon/{idSalon}/products', name: 'grooming_salon_products')]
    public function index_products($idSalon): Response
    {
        $groomer = $this->security->getUser();

        $groomingSalon = $this->salonRepository->findOneById($idSalon);
        $products = $this->productRepository->findBy(['groomingSalon' => $groomingSalon]);

        return $this->render(
            'groomers/groomingSalon/products/products.html.twig',
            [
                'products' => $products,
                'idSalon' => $idSalon
            ]
        );
    }
    #[Route('/groomers/dashboard/grooming-salon/{idSalon}/products-create', name: 'grooming_salon_create_products')]
    public function create_products($idSalon, Request $request): Response
    {
        $groomer = $this->security->getUser();
        $groomingSalon = $this->salonRepository->findOneBy(['id' => $idSalon, 'userGroomer' => $groomer]);

        $product = new Product();
        $product->setGroomingSalon($groomingSalon);

        $formProduct = $this->createForm(ProductType::class, $product);

        $formProduct->handleRequest($request);
        if ($formProduct->isSubmitted() && $formProduct->isValid()) {
            $file = $formProduct['urlPicture']->getData();
            $filename = "product" . uniqid() . "-groomer" . $this->security->getUser()->getId() . '.png';

            try {
                $file->move(
                    "./images/products-img/",
                    $filename
                );
                $this->addFlash('success', "Image ajoutée");
            } catch (FileException $e) {
                $this->addFlash('danger', "Erreur survenue au niveau de l'image: " . $e->getMessage());
                return $this->redirectToRoute('grooming_salon_create_products', ['idSalon' => $idSalon]);
            }

            $product = $formProduct->getData();
            $product->setUrlPicture($filename);

            $this->em->persist($product);
            $this->em->flush();
            $this->addFlash('success', "Produit ajouté avec succès !");

            return $this->redirectToRoute('grooming_salon_products', ['idSalon' => $idSalon]);
        }
        return $this->render(
            'groomers/groomingSalon/products/create_product.html.twig',
            [
                'formProduct' => $formProduct->createView()
            ]
        );
    }
}
