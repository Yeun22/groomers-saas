<?php

namespace App\Controller\Groomer;

use App\Entity\PictureShowroom;
use App\Entity\Showroom;
use App\Repository\PictureShowroomRepository;
use App\Repository\UserGroomerRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ShowroomPicturesController extends AbstractController
{
    public function __construct(
        private Security $security,
        private UserGroomerRepository $groomerRepository,
        private EntityManagerInterface $em,
        private PictureShowroomRepository $pictureRepository
    ) {
    }
    #[Route('/groomers/dashboard/{idGroomer}/pictures-showroom', name: 'add_showroom_picture')]
    public function add_showroom_picture($idGroomer, Request $request)
    {
        /** @var UserGroomer */
        $groomer = $this->groomerRepository->findOneById($idGroomer);

        // First Check if it's good user
        if ($this->security->getUser() !== $groomer) {
            $this->addFlash("warning", "Something strange happened ! Where are you go ?");
            return $this->redirectToRoute('app_logout');
        }
        $pictures = null;
        if ($groomer->getShowroom() !== null) {
            $pictures = $groomer->getShowroom()->getPicturesShowroom();
        }
        if ($request->getMethod() === 'POST') {
            $files = $request->files->get('pictures');
            $descriptions = $request->request->get('descriptions');

            $this->security->getUser()->getShowroom() === null ? $showroom = new Showroom() : $showroom = $this->security->getUser()->getShowroom();

            $this->em->persist($showroom);
            for ($i = 1; $i <= 11; $i++) {
                if ($files["picture$i"] !== null && $descriptions["picture$i"] !== "") {
                    $picture = new PictureShowroom();
                    $pictureName = "showroomPicture$i-user" . $this->security->getUser()->getId() . '.png';
                    try {
                        if (file_exists(
                            "./images/showroom-pictures/" .
                                $pictureName
                        )) {
                            unlink(
                                "./images/showroom-pictures/" .
                                    $pictureName
                            );
                        }
                        $files["picture$i"]->move(
                            "./images/showroom-pictures/",
                            $pictureName
                        );
                        $this->addFlash('success', "Picture $i Updated");
                    } catch (FileException $e) {
                        $this->addFlash('danger', "Erreur survenue : " . $e->getMessage());
                    }
                    $picture->setDescription($descriptions["picture$i"])
                        ->setUrlPicture($pictureName);
                    $this->em->persist($picture);
                    $showroom->addPicturesShowroom($picture);
                }
            }
            if (!empty($showroom->getPicturesShowroom())) {
                $this->security->getUser()->setShowroom($showroom);
            }
            $this->em->flush();
            $this->addFlash('success', "Your shoroom is updated !");
            return $this->redirectToRoute('add_showroom_picture', ['idGroomer' => $idGroomer]);
        }


        return $this->render(
            'groomers/website/manage_showwroom_pictures.html.twig',
            [
                'pictures' => $pictures
            ]
        );
    }

    #[Route('/groomers/dashboard/{idGroomer}/picture/{idPicture}/delete', name: 'delete_showroom_picture')]
    public function delete_showroom_picture($idGroomer, $idPicture, Request $request, Filesystem $filesystem)
    {
        /** @var UserGroomer */
        $groomer = $this->groomerRepository->findOneById($idGroomer);
        $showroom = $groomer->getShowroom();
        $picture = $this->pictureRepository->findOneById($idPicture);
        $showroom->removePicturesShowroom($picture);
        if ($filesystem->exists("./images/showroom-pictures/" . $picture->getUrlPicture())) {
            $filesystem->remove(["./images/showroom-pictures/" . $picture->getUrlPicture()]);
        }
        $this->em->flush();

        $this->addFlash('success', 'La photo est bien supprimée');
        return $this->redirectToRoute('add_showroom_picture', ['idGroomer' => $idGroomer]);
    }
}
