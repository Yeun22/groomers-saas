<?php

namespace App\Controller;

use App\Entity\UserCustomer;
use App\Entity\UserGeneral;
use App\Entity\UserGroomer;
use App\Form\RegistrationCustomerFormType;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\RegistrationGroomerFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class RegistrationController extends AbstractController
{

    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher,
        private EntityManagerInterface $entityManager,
        private UserAuthenticatorInterface $userAuthenticatorInterface
    ) {
    }

    #[Route('/register/groomer', name: 'groomers_register')]
    public function register(Request $request): Response
    {
        $user = new UserGroomer();
        $form = $this->createForm(RegistrationGroomerFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $this->userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $this->entityManager->persist($user);
            $this->entityManager->flush();
            // do anything else you need here, like send an email
            $this->addFlash('success', 'Register Success!');

            return $this->redirectToRoute('app_home');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
    #[Route('/register/customer', name: 'customers_register')]
    public function register_customer(Request $request): Response
    {
        $user = new UserCustomer();
        $form = $this->createForm(RegistrationCustomerFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $this->userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $this->entityManager->persist($user);
            $this->entityManager->flush();
            // do anything else you need here, like send an email
            $this->addFlash('success', 'Register Success!');

            return $this->redirectToRoute('app_home');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
