<?php

namespace App\Form;

use App\Entity\WebsiteInformations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\NotNull;

class WebsiteInformationsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('textPresentation', TextareaType::class, [
                'constraints' => [
                    new Length([
                        'min' => 50,
                        'minMessage' => 'Your presentation should be at least {{ limit }} characters',
                        'max' => 1500,
                        'maxMessage' => 'Your presentation is a litle bit to loong no ? max {{ limit }} characters allowed.',
                    ]),
                ],
                'label' => 'A text to introduce yourself :',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('emailContact', EmailType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Please enter an address email',
                    ]),
                ],
                'label' => 'Email to contact you :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "mylovelysalon@mail.com"
                ],
            ])
            ->add('linkFacebook', TextType::class, [
                'label' => 'Link to Facebook Page :',
                'attr' => [
                    'class' => 'form-control',
                ],
                'required'   => false,
            ])
            ->add('linkInstagram', TextType::class, [
                'label' => 'Link to Instagram :',
                'attr' => [
                    'class' => 'form-control',
                ],
                'required'   => false,
            ])
            ->add('linkTiktok', TextType::class, [
                'label' => 'Link to TikTok :',
                'attr' => [
                    'class' => 'form-control',
                ],
                'required'   => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => WebsiteInformations::class,
        ]);
    }
}
