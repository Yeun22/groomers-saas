<?php

namespace App\Form;

use App\Entity\HoraireWeek;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class HoraireWeekFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Monday', TextType::class, [
                'constraints' => $options['hourly_constraints'],
                'label' => 'Monday :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "9h -18h"
                ],
            ])
            ->add('Tuesday', TextType::class, [
                'constraints' => $options['hourly_constraints'],
                'label' => 'Tuesday :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "9h -18h"
                ],
            ])
            ->add('Wednesday', TextType::class, [
                'constraints' => $options['hourly_constraints'],
                'label' => 'Wednesday :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "9h -18h"
                ],
            ])
            ->add('Thursday', TextType::class, [
                'constraints' => $options['hourly_constraints'],
                'label' => 'Thursday :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "9h -18h"
                ],
            ])
            ->add('Friday', TextType::class, [
                'constraints' => $options['hourly_constraints'],
                'label' => 'Friday :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "9h -18h"
                ],
            ])
            ->add('Saturday', TextType::class, [
                'constraints' => $options['hourly_constraints'],
                'label' => 'Saturday :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "9h -13h"
                ],
            ])
            ->add('Sunday', TextType::class, [
                'constraints' => $options['hourly_constraints'],
                'label' => 'Sunday :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "Closed"
                ],
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-lg btn-secondary mt-3',
                    'value' => "Register This"
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => HoraireWeek::class,
            'hourly_constraints' =>  [
                new NotBlank([
                    'message' => 'Please your hourly for this day. If your closed, write "Closed".',
                ]),
                new Length([
                    'min' => 5,
                    'minMessage' => 'Your hourly need {{ limit }} characters',
                    'max' => 20,
                    'maxMessage' => 'Your hourly is very looooong no ?',
                ]),
            ],
        ]);
    }
}
