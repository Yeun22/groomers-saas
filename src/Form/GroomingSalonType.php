<?php

namespace App\Form;

use App\Entity\GroomingSalon;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GroomingSalonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter name of your grooming salon',
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Your name should be at least {{ limit }} characters',
                        'max' => 850,
                        'maxMessage' => 'Your name is very looooong no ?',
                    ]),
                ],
                'label' => 'Name of your Grooming Salon :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "Lovely Dogs Groomer"
                ],
            ])
            ->add('addressLine1', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your address',
                    ]),
                ],
                'label' => 'Address Line 1 :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "25 street of Canebirge"
                ],
            ])
            ->add('addressLine2', TextType::class, [
                'label' => 'Address Line 2 :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "if you need more line",
                ],
                'required'   => false,
            ])
            ->add('postalCode', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your postal code',
                    ]),
                ],
                'label' => 'ZIP (Postal Code):',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "22000"
                ],
            ])
            ->add('city', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your city',
                    ]),
                ],
                'label' => 'City :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "London"
                ],
            ])
            ->add('country', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your country',
                    ]),
                ],
                'label' => 'Country :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "ENGLAND"
                ],
            ])
            ->add('phoneNumber', TelType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your phone number',
                    ]),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Your phone number should be at least {{ limit }} characters',
                        'max' => 16,
                        'maxMessage' => 'this is a strange number phone',

                    ]),
                ],
                'label' => 'Phone Number :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "0643XXXXXX"
                ],

            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GroomingSalon::class,
        ]);
    }
}
