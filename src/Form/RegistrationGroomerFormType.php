<?php

namespace App\Form;

use App\Entity\UserGroomer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationGroomerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('phoneNumber', TelType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your phone number',
                    ]),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Your phone number should be at least {{ limit }} characters',
                        'max' => 16,
                        'maxMessage' => 'this is a strange number phone',

                    ]),
                ],
                'label' => 'Phone Number :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "0643XXXXXX"
                ],

            ])
            ->add('firstname', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your firstname',
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Your name should be at least {{ limit }} characters',
                        'max' => 200,
                        'maxMessage' => 'Your name is very looooong no ?',
                    ]),
                ],
                'label' => 'Firstname :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "John"
                ],
            ])
            ->add('lastname', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your lastname',
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Your name should be at least {{ limit }} characters',
                        'max' => 200,
                        'maxMessage' => 'Your name is very looooong no ?',
                    ]),
                ],
                'label' => 'Lastname :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "Doe"
                ],
            ])
            ->add('email', EmailType::class, [
                'constraints' =>  new NotBlank([
                    'message' => 'Please enter your email',
                ]),
                'label' => 'Email :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "johndoe@mail.com"
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add(
                'sex',
                ChoiceType::class,
                [
                    'choices' => [
                        'Femme' => 'femme',
                        'Homme' => 'homme',
                    ],
                    'attr' => [
                        'class' => 'form-select'
                    ],
                    'label' => 'Sex :'
                ]
            )
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "Password min 6 chars ..."
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters and less than 100',
                        // max length allowed for security reasons
                        'max' => 100,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserGroomer::class,
        ]);
    }
}
