<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use App\Repository\ProductCategorieRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ProductType extends AbstractType
{
    public function __construct(private ProductCategorieRepository $categorieRepository)
    {
        $this->categories = $categorieRepository->findAll();
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter name of this product',
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Name should be at least {{ limit }} characters',
                        'max' => 50,
                        'maxMessage' => 'This name is very looooong no ?',
                    ]),
                ],
                'label' => 'Name of Product :',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => "My Awesome Product"
                ],
            ])->add(
                'price',
                NumberType::class,
                [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter min price of this product',
                        ]),
                    ],
                    'label' => 'Min price for this Product :',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => "My Awesome Product"
                    ],
                ]
            )
            ->add('urlPicture', FileType::class, [
                'label' => 'Picture for this Product :',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('categorie', ChoiceType::class, [
                'choice_label' => 'name',
                'choices' => $this->categories,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
