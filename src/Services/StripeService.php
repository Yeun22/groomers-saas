<?php

namespace App\Services;

use App\Entity\Purchase;

class StripeService
{

    public function __construct(protected string $secretKey, protected string $publicKey)
    {
    }

    public function getPaymentIntent(Purchase $purchase)
    {
        \Stripe\Stripe::setApiKey($this->secretKey);

        return \Stripe\PaymentIntent::create([
            'amount' => $purchase->getTotalamount(), //Il faut des centimes et on les stockes en cents donc c'est cool
            'currency' => 'eur'
        ]);
    }

    public function getPublicKey(): string
    {
        return $this->publicKey;
    }
}
