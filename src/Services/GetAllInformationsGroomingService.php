<?php

namespace App\Services;

use Exception;
use App\Repository\GroomingSalonRepository;
use App\Repository\ProductCategorieRepository;
use App\Repository\ProductRepository;
use App\Repository\UserGroomerRepository;
use App\Repository\WebsiteInformationsRepository;

class GetAllInformationsGroomingService
{
    public function __construct(
        private GroomingSalonRepository $groomingSalonRepository,
        private UserGroomerRepository $userGroomerRepository,
        private WebsiteInformationsRepository $websiteInformationsRepository,
        private ProductCategorieRepository $productCategorieRepository,
        private ProductRepository $productRepository
    ) {
    }

    /**
     * @var int idSalon
     * @var int idGroomer
     */
    public function renderAllInformationInSalon(int $idSalon, int $idGroomer): array
    {
        $groomingSalon = [];
        $groomer = $this->userGroomerRepository->findOneById($idGroomer);
        $salon = $this->groomingSalonRepository->findOneBy(["id" => $idSalon, 'userGroomer' => $groomer]);
        if ($salon->getWebsiteInformations()[0] === null) {
            throw new Exception("This groomer doesn't have eshop website");
        }
        $websiteId = $salon->getWebsiteInformations()[0]->getId();
        $website = $this->websiteInformationsRepository->findOneById($websiteId);
        $allProducts = $salon->getProducts()->toArray();
        $categories = [];
        foreach ($this->productCategorieRepository->findAll() as $c) {
            $products = $this->productRepository->findBy(['groomingSalon' => $salon, 'categorie' => $c], null, 6);

            $categories[] = ['name' => $c->getName(), 'products' => $products];
        }
        $groomingSalon = [
            'groomer' => $groomer,
            'salon' => $salon,
            "website" => $website,
            'categories' => $categories,
            'products' => $allProducts
        ];

        return $groomingSalon;
    }
}
