<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class PathChangeEventListener implements EventSubscriberInterface
{
    public function __construct(private UrlGeneratorInterface $url, private Security $security)
    {
    }
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 32]],
        ];
    }
    public function onKernelRequest(RequestEvent $event,)
    {
        $request = $event->getRequest();

        // if (str_starts_with($request->getPathInfo(), '/groomers/') && $this->security->getUser() === null) {
        //     return  $event->setResponse(new RedirectResponse($this->url->generate('app_login')));
        // }
    }
}
