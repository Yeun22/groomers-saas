<?php

namespace App\Purchase;

use DateTime;
use App\Entity\Purchase;
use App\Cart\CartService;
use App\Entity\PurchaseItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class PurchasePersister
{

    protected $security;
    protected $cartService;
    protected $em;

    public function __construct(Security $security, EntityManagerInterface $em, CartService $cartService)
    {
        $this->security = $security;
        $this->cartService = $cartService;
        $this->em = $em;
    }

    public function storePurchase(Purchase $purchase)
    {

        //6 Nous allons la lié à l'user
        $purchase->setUserCustomer($this->security->getUser());
        $this->em->persist($purchase);

        $totalPurchase = 0;

        //7 Lier avec les produits 
        foreach ($this->cartService->getDetailedCartItems() as $cartItem) {
            $purchaseItem = new PurchaseItem;
            $purchaseItem->setPurchase($purchase)
                ->setProduct($cartItem->product)
                ->setProductName($cartItem->product->getName())
                ->setQuantity($cartItem->qty)
                ->setTotalAmount($cartItem->getTotal())
                ->setProductPrice($cartItem->product->getPrice());

            $this->em->persist($purchaseItem);

            $totalPurchase += $purchaseItem->getTotalAmount();
        }
        $purchase->setTotalAmount($totalPurchase);
        $purchase->setStatus(Purchase::STATUS_PENDING);
        $this->em->flush();
    }
}
