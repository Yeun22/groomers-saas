<?php

namespace App\Twig;

use App\Repository\GroomingSalonRepository;
use App\Repository\ProductCategorieRepository;
use App\Services\GetAllInformationsGroomingService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    protected $categoryRepository;
    protected $groomingSalonService;

    public function __construct(
        ProductCategorieRepository $categoryRepository,
        GroomingSalonRepository $groomingSalonRepository,
        GetAllInformationsGroomingService $groomingSalonService
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->groomingSalonService = $groomingSalonService;
        $this->groomingSalonRepository = $groomingSalonRepository;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('categoryNavbar', [$this, 'category']),
            new TwigFunction('renderSalonInformation', [$this, 'renderSalonInformation'])
        ];
    }

    public function category(): array
    {
        return $this->categoryRepository->findAll();
    }

    public function renderSalonInformation(int $idSalon, int $idGroomer)
    {
        $salon = $this->groomingSalonRepository->findOneBy(["id" => $idSalon, 'userGroomer' => $idGroomer]);
        return $salon;
    }
}
