<?php

namespace App\Entity;

use App\Repository\WebsiteInformationsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WebsiteInformationsRepository::class)]
class WebsiteInformations
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $textPresentation;

    #[ORM\Column(type: 'string', length: 255)]
    private $emailContact;

    #[ORM\Column(type: 'string', length: 255)]
    private $linkFacebook;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $linkInstagram;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $linkTiktok;

    #[ORM\Column(type: 'array')]
    // private $colors = [
    //     "mainBg" => "AliceBlue",
    //     "secondBg" => "Lavender",
    //     "mainText" => "black",
    //     "primary" => "Tomato",
    //     "secondary" => "RoyalBlue"
    // ];
    private $colors = [

        0 => ['colors-mainBg' => 'AliceBlue'],
        1 => ['colors-secondBg'  => 'Lavender'],
        2 => ['colors-mainText'  => 'black'],
        3 => ['colors-primary'  => 'Tomato'],
        4 => ['colors-secondary'  => 'RoyalBlue'],
    ];

    #[ORM\ManyToOne(targetEntity: GroomingSalon::class, inversedBy: 'websiteInformations', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private $groomingSalon;

    #[ORM\Column(type: 'string', length: 255)]
    private $logo = "logo-blue.png";

    #[ORM\Column(type: 'string', length: 255)]
    private $profilPicture = "logo-red.png";

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTextPresentation(): ?string
    {
        return $this->textPresentation;
    }

    public function setTextPresentation(string $textPresentation): self
    {
        $this->textPresentation = $textPresentation;

        return $this;
    }

    public function getEmailContact(): ?string
    {
        return $this->emailContact;
    }

    public function setEmailContact(string $emailContact): self
    {
        $this->emailContact = $emailContact;

        return $this;
    }

    public function getLinkFacebook(): ?string
    {
        return $this->linkFacebook;
    }

    public function setLinkFacebook(string $linkFacebook): self
    {
        $this->linkFacebook = $linkFacebook;

        return $this;
    }

    public function getLinkInstagram(): ?string
    {
        return $this->linkInstagram;
    }

    public function setLinkInstagram(?string $linkInstagram): self
    {
        $this->linkInstagram = $linkInstagram;

        return $this;
    }

    public function getLinkTiktok(): ?string
    {
        return $this->linkTiktok;
    }

    public function setLinkTiktok(?string $linkTiktok): self
    {
        $this->linkTiktok = $linkTiktok;

        return $this;
    }

    public function getColors(): ?array
    {
        return $this->colors;
    }

    public function setColors(array $colors): self
    {
        $this->colors = $colors;

        return $this;
    }

    public function getGroomingSalon(): ?GroomingSalon
    {
        return $this->groomingSalon;
    }

    public function setGroomingSalon(?GroomingSalon $groomingSalon): self
    {
        $this->groomingSalon = $groomingSalon;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getProfilPicture(): ?string
    {
        return $this->profilPicture;
    }

    public function setProfilPicture(string $profilPicture): self
    {
        $this->profilPicture = $profilPicture;

        return $this;
    }
}
