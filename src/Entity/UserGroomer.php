<?php

namespace App\Entity;

use App\Entity\UserGeneral;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserGroomerRepository;

#[ORM\Entity(repositoryClass: UserGroomerRepository::class)]
#[ORM\Table(name: 'user_groomer')]
class UserGroomer extends UserGeneral
{
    #[ORM\Column(type: 'string', length: 255)]
    private $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    private $lastname;

    #[ORM\Column(type: 'string', length: 255)]
    private $email;

    #[ORM\Column(type: 'boolean')]
    private $access_site = false;

    #[ORM\Column(type: 'boolean')]
    private $access_crm = false;

    #[ORM\Column(type: 'boolean')]
    private $access_ecommerce = false;

    #[ORM\OneToMany(mappedBy: 'userGroomer', targetEntity: GroomingSalon::class, orphanRemoval: true)]
    private $groomingSalons;

    #[ORM\Column(type: 'string')]
    private $sex = "femme";

    #[ORM\OneToOne(inversedBy: 'userGroomer', targetEntity: Showroom::class, cascade: ['persist', 'remove'])]
    private $showroom;

    public function __construct()
    {
        parent::__construct();
        $this->groomingSalons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAccessSite(): ?bool
    {
        return $this->access_site;
    }

    public function setAccessSite(bool $access_site): self
    {
        $this->access_site = $access_site;

        return $this;
    }

    public function getAccessCrm(): ?bool
    {
        return $this->access_crm;
    }

    public function setAccessCrm(bool $access_crm): self
    {
        $this->access_crm = $access_crm;

        return $this;
    }

    public function getAccessEcommerce(): ?bool
    {
        return $this->access_ecommerce;
    }

    public function setAccessEcommerce(bool $access_ecommerce): self
    {
        $this->access_ecommerce = $access_ecommerce;

        return $this;
    }

    /**
     * @return Collection<int, GroomingSalon>
     */
    public function getGroomingSalons(): Collection
    {
        return $this->groomingSalons;
    }

    public function addGroomingSalon(GroomingSalon $groomingSalon): self
    {
        if (!$this->groomingSalons->contains($groomingSalon)) {
            $this->groomingSalons[] = $groomingSalon;
            $groomingSalon->setUserGroomer($this);
        }

        return $this;
    }

    public function removeGroomingSalon(GroomingSalon $groomingSalon): self
    {
        if ($this->groomingSalons->removeElement($groomingSalon)) {
            // set the owning side to null (unless already changed)
            if ($groomingSalon->getUserGroomer() === $this) {
                $groomingSalon->setUserGroomer(null);
            }
        }

        return $this;
    }

    public function getSex()
    {
        return $this->sex;
    }

    public function setSex($sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getShowroom(): ?Showroom
    {
        return $this->showroom;
    }

    public function setShowroom(?Showroom $showroom): self
    {
        $this->showroom = $showroom;

        return $this;
    }
}
