<?php

namespace App\Entity;

use App\Repository\ServiceCategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ServiceCategorieRepository::class)]
class ServiceCategorie
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $picture;

    #[ORM\ManyToMany(targetEntity: GroomingSalon::class, mappedBy: 'servicesCategories')]
    private $groomingSalons;

    public function __construct()
    {
        $this->groomingSalons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return Collection<int, GroomingSalon>
     */
    public function getGroomingSalons(): Collection
    {
        return $this->groomingSalons;
    }

    public function addGroomingSalon(GroomingSalon $groomingSalon): self
    {
        if (!$this->groomingSalons->contains($groomingSalon)) {
            $this->groomingSalons[] = $groomingSalon;
            $groomingSalon->addServicesCategory($this);
        }

        return $this;
    }

    public function removeGroomingSalon(GroomingSalon $groomingSalon): self
    {
        if ($this->groomingSalons->removeElement($groomingSalon)) {
            $groomingSalon->removeServicesCategory($this);
        }

        return $this;
    }
}
