<?php

namespace App\Entity;

use App\Entity\UserGeneral;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserCustomerRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: UserCustomerRepository::class)]
#[ORM\Table(name: 'user_customer')]
class UserCustomer extends UserGeneral
{
    #[ORM\Column(type: 'string', length: 255)]
    private $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    private $lastname;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $email;

    #[ORM\ManyToMany(targetEntity: GroomingSalon::class, inversedBy: 'userCustomers')]
    private $salons;

    #[ORM\Column(type: 'string', length: 255)]
    private $sex;

    #[ORM\OneToMany(mappedBy: 'userCustomer', targetEntity: Purchase::class, orphanRemoval: true)]
    private $purchases;

    public function __construct()
    {
        parent::__construct();
        $this->salons = new ArrayCollection();
        $this->purchases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, GroomingSalon>
     */
    public function getSalons(): Collection
    {
        return $this->salons;
    }

    public function addSalon(GroomingSalon $salon): self
    {
        if (!$this->salons->contains($salon)) {
            $this->salons[] = $salon;
        }

        return $this;
    }

    public function removeSalon(GroomingSalon $salon): self
    {
        $this->salons->removeElement($salon);

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * @return Collection<int, Purchase>
     */
    public function getPurchases(): Collection
    {
        return $this->purchases;
    }

    public function addPurchase(Purchase $purchase): self
    {
        if (!$this->purchases->contains($purchase)) {
            $this->purchases[] = $purchase;
            $purchase->setUserCustomer($this);
        }

        return $this;
    }

    public function removePurchase(Purchase $purchase): self
    {
        if ($this->purchases->removeElement($purchase)) {
            // set the owning side to null (unless already changed)
            if ($purchase->getUserCustomer() === $this) {
                $purchase->setUserCustomer(null);
            }
        }

        return $this;
    }
}
