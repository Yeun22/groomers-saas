<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\WebsiteInformations;
use App\Repository\GroomingSalonRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: GroomingSalonRepository::class)]
class GroomingSalon
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToOne(targetEntity: UserGroomer::class, inversedBy: 'groomingSalons', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private $userGroomer;

    #[ORM\Column(type: 'string', length: 255)]
    private $addressLine1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $addressLine2;

    #[ORM\Column(type: 'string', length: 15)]
    private $postalCode;

    #[ORM\Column(type: 'string', length: 255)]
    private $city;

    #[ORM\Column(type: 'string', length: 255)]
    private $country;

    #[ORM\Column(type: 'string', length: 15)]
    private $phoneNumber;

    #[ORM\Column(type: 'array')]
    private $hourly = [];

    #[ORM\OneToMany(mappedBy: 'groomingSalon', targetEntity: WebsiteInformations::class, orphanRemoval: true, cascade: ['persist'])]
    private $websiteInformations;

    #[ORM\ManyToMany(targetEntity: ServiceCategorie::class, inversedBy: 'groomingSalons')]
    private $servicesCategories;

    #[ORM\OneToMany(mappedBy: 'groomingSalon', targetEntity: Product::class, orphanRemoval: true)]
    private $products;

    #[ORM\ManyToMany(targetEntity: UserCustomer::class, mappedBy: 'salons')]
    private $userCustomers;

    public function __construct()
    {
        $this->websiteInformations = new ArrayCollection();
        $this->servicesCategories = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->userCustomers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUserGroomer(): ?UserGroomer
    {
        return $this->userGroomer;
    }

    public function setUserGroomer(?UserGroomer $userGroomer): self
    {
        $this->userGroomer = $userGroomer;

        return $this;
    }
    public function getAddressLine1(): ?string
    {
        return $this->addressLine1;
    }

    public function setAddressLine1(string $addressLine1): self
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    public function getAddressLine2(): ?string
    {
        return $this->addressLine2;
    }

    public function setAddressLine2(?string $addressLine2): self
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }


    public function getHourly(): ?array
    {
        return $this->hourly;
    }

    public function setHourly(array $hourly): self
    {
        $this->hourly = $hourly;

        return $this;
    }

    /**
     * @return Collection<int, WebsiteInformations>
     */
    public function getWebsiteInformations(): Collection
    {
        return $this->websiteInformations;
    }

    public function addWebsiteInformations(WebsiteInformations $websiteInformations): self
    {
        if (!$this->websiteInformations->contains($websiteInformations)) {
            $this->websiteInformations[] = $websiteInformations;
            $websiteInformations->setGroomingSalon($this);
        }

        return $this;
    }

    public function removeWebsiteInformations(WebsiteInformations $websiteInformations): self
    {
        if ($this->websiteInformations->removeElement($websiteInformations)) {
            // set the owning side to null (unless already changed)
            if ($websiteInformations->getGroomingSalon() === $this) {
                $websiteInformations->setGroomingSalon(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ServiceCategorie>
     */
    public function getServicesCategories(): Collection
    {
        return $this->servicesCategories;
    }

    public function addServicesCategory(ServiceCategorie $servicesCategory): self
    {
        if (!$this->servicesCategories->contains($servicesCategory)) {
            $this->servicesCategories[] = $servicesCategory;
        }

        return $this;
    }

    public function removeServicesCategory(ServiceCategorie $servicesCategory): self
    {
        $this->servicesCategories->removeElement($servicesCategory);

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setGroomingSalon($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getGroomingSalon() === $this) {
                $product->setGroomingSalon(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserCustomer>
     */
    public function getUserCustomers(): Collection
    {
        return $this->userCustomers;
    }

    public function addUserCustomer(UserCustomer $userCustomer): self
    {
        if (!$this->userCustomers->contains($userCustomer)) {
            $this->userCustomers[] = $userCustomer;
            $userCustomer->addSalon($this);
        }

        return $this;
    }

    public function removeUserCustomer(UserCustomer $userCustomer): self
    {
        if ($this->userCustomers->removeElement($userCustomer)) {
            $userCustomer->removeSalon($this);
        }

        return $this;
    }
}
