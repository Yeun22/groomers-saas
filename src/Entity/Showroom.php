<?php

namespace App\Entity;

use App\Repository\ShowroomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ShowroomRepository::class)]
class Showroom
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'showroom', targetEntity: PictureShowroom::class, orphanRemoval: true)]
    private $pictures_showroom;

    #[ORM\OneToOne(mappedBy: 'showroom', targetEntity: UserGroomer::class, cascade: ['persist', 'remove'])]
    private $userGroomer;

    public function __construct()
    {
        $this->pictures_showroom = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, PictureShowroom>
     */
    public function getPicturesShowroom(): Collection
    {
        return $this->pictures_showroom;
    }

    public function addPicturesShowroom(PictureShowroom $picturesShowroom): self
    {
        if (!$this->pictures_showroom->contains($picturesShowroom)) {
            $this->pictures_showroom[] = $picturesShowroom;
            $picturesShowroom->setShowroom($this);
        }

        return $this;
    }

    public function removePicturesShowroom(PictureShowroom $picturesShowroom): self
    {
        if ($this->pictures_showroom->removeElement($picturesShowroom)) {
            // set the owning side to null (unless already changed)
            if ($picturesShowroom->getShowroom() === $this) {
                $picturesShowroom->setShowroom(null);
            }
        }

        return $this;
    }

    public function getUserGroomer(): ?UserGroomer
    {
        return $this->userGroomer;
    }

    public function setUserGroomer(?UserGroomer $userGroomer): self
    {
        // unset the owning side of the relation if necessary
        if ($userGroomer === null && $this->userGroomer !== null) {
            $this->userGroomer->setShowroom(null);
        }

        // set the owning side of the relation if necessary
        if ($userGroomer !== null && $userGroomer->getShowroom() !== $this) {
            $userGroomer->setShowroom($this);
        }

        $this->userGroomer = $userGroomer;

        return $this;
    }
}
